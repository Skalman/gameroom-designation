export function arrayToggle<T>(arr: T[], item: T) {
  if (arr.includes(item)) {
    return arr.filter((x) => x !== item);
  } else {
    return [...arr, item];
  }
}
