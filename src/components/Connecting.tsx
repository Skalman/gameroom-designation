import type { DesignationGame } from "../DesignationGame";
import { useGameState } from "../useGameState";

export function Connecting({ game }: { game: DesignationGame }) {
  const state = useGameState(game);
  return <div>Connecting {JSON.stringify(state)}</div>;
}
