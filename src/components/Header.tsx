import type { DesignationGame } from "../DesignationGame";
import { isPlayingState } from "../fsm";
import { useGameState } from "../useGameState";

export function Header({ game }: { game: DesignationGame }) {
  const state = useGameState(game, (x) => x.state);

  const isPlaying = isPlayingState(state);

  if (isPlaying) {
    return (
      <header className="header header-isplaying mb-2 mb-xl-3 bg-dark text-white text-center"></header>
    );
  } else {
    return (
      <header className="header mb-2 mb-xl-3 bg-dark text-white text-center px-3 py-2">
        <h1 className="transition display-3">Designation</h1>
      </header>
    );
  }
}
