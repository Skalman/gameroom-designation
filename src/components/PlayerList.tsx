import type { Player } from "@game-room/client";

import type { DesignationGameData } from "../models";
import * as Ui from "./ui";

export function PlayerList({
  players,
  highlightedPlayerId,
}: {
  players: readonly Player<DesignationGameData>[];
  highlightedPlayerId?: number;
}) {
  if (!players.length) {
    return (
      <p>
        <i>No players yet</i>
      </p>
    );
  }

  return (
    <Ui.ListInline>
      {players.map((x) => (
        <Ui.ListInlineItem
          key={x.id}
          className={`badge bg-light text-dark border ${
            highlightedPlayerId === x.id ? "border-success" : ""
          }`}
        >
          {x.name}
        </Ui.ListInlineItem>
      ))}
    </Ui.ListInline>
  );
}
