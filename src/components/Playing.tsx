import type { DesignationGame } from "../DesignationGame";
import { useGameState } from "../useGameState";
import { GameLog } from "./GameLog";
import { GameStateDisplay } from "./GameStateDisplay";
import { GameStats } from "./GameStats";
import { Hint } from "./Hint";
import { PlayerList } from "./PlayerList";
import * as Ui from "./ui";
import { WordTable } from "./WordTable";

export function Playing({ game }: { game: DesignationGame }) {
  const { calculated, state, players, playersByTeam, me } = useGameState(game);

  if (!calculated || !players || !playersByTeam || !me) {
    return null;
  }

  return (
    <div>
      <div className="d-flex">
        <div className="w-50 flex-grow-1">
          <WordTable game={game} words={calculated.words} />
          <GameStateDisplay state={state} />
          <Hint game={game} />
        </div>
        <div>
          <GameStats calculated={calculated} />
          <p>
            <Ui.ButtonOutlinePrimarySmall
              onClick={() =>
                navigator.clipboard.writeText(window.location.href)
              }
            >
              Copy invitation link
            </Ui.ButtonOutlinePrimarySmall>
          </p>
          <h5>Your team</h5>
          <PlayerList
            players={playersByTeam[me.team]}
            highlightedPlayerId={me.playerId}
          />
          <h5>The other team</h5>
          <PlayerList players={playersByTeam[me.otherTeam]} />
          <GameLog game={game} />
        </div>
      </div>
    </div>
  );
}
