import type { DesignationGame } from "../DesignationGame";
import type { Word as WordModel } from "../models";
import { useGameState } from "../useGameState";
import { Word } from "./Word";

const five = [0, 1, 2, 3, 4];

export function WordTable({
  game,
  words,
}: {
  game: DesignationGame;
  words: WordModel[];
}) {
  const { state } = useGameState(game);

  return (
    <table className="mx-auto mb-3">
      <tbody>
        {five.map((y) => (
          <tr key={y}>
            {five.map((x) => {
              const word = words[y * 5 + x];
              return (
                <td key={x}>
                  <Word game={game} word={word} state={state} />
                </td>
              );
            })}
          </tr>
        ))}
      </tbody>
    </table>
  );
}
