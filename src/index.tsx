import React from "react";
import ReactDOM from "react-dom";

import { Designation } from "./components/Designation";
import { WEBSOCKET_URL } from "./environment";

ReactDOM.render(
  <React.StrictMode>
    <Designation url={WEBSOCKET_URL} />
  </React.StrictMode>,
  document.getElementById("root")
);
