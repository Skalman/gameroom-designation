#!/usr/bin/env -S deno run --allow-run --unstable

/* global Deno */

const commands = {
  startClient: "npm run start:client",
  startServer: "npm run start:server",
};

const processes = Object.values(commands).map((cmd) =>
  Deno.run({
    cmd: cmd.split(" "),
    stdout: "inherit",
    stderr: "inherit",
  })
);

Promise.any(processes.map((x) => x.status())).then((result) => {
  for (const process of processes) {
    process.close();
    process.kill(9);
  }

  Deno.exit(result.code);
});
