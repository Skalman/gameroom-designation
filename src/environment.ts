export const WEBSOCKET_URL = env.WEBSOCKET_URL;

declare global {
  export const env: {
    WEBSOCKET_URL: string;
  };
}
