import { useCallback, useEffect, useState } from "react";

import type { DesignationGame } from "./DesignationGame";
import type { GameState } from "./models";

export function useGameState<T>(
  game: DesignationGame,
  selector: (x: GameState) => T
): T;
export function useGameState(game: DesignationGame): GameState;
export function useGameState<T>(
  game: DesignationGame,
  selector: (x: GameState) => T = (x) => (x as unknown) as T
) {
  const [val, setVal] = useState(() => selector(game.state));

  const listener = useCallback(() => {
    setVal((val) => {
      const selected = selector(game.state);
      return shallowEqual(val, selected) ? val : selected;
    });
    // The selector is could theoretically have other dependencies, but it shouldn't.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [game.state]);

  useEffect(() => {
    game.addListener(listener);

    return () => {
      game.removeListener(listener);
    };
  }, [game, listener]);

  return val;
}

function shallowEqual<T>(a: T, b: T) {
  if (Object.is(a, b)) {
    return true;
  } else if (!a || !b || typeof a !== "object" || typeof b !== "object") {
    return false;
  } else if (Array.isArray(a) && Array.isArray(b)) {
    return a.length === b.length && a.every((x, i) => b[i] === x);
  } else {
    for (const key in a) {
      if (a[key] !== b[key]) {
        return false;
      }
    }
    return true;
  }
}
