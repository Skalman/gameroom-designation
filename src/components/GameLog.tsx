import type { Player } from "@game-room/client";

import type { DesignationGame } from "../DesignationGame";
import type { CalculatedLogItem, DesignationGameData, Word } from "../models";
import { useGameState } from "../useGameState";

export function GameLog({ game }: { game: DesignationGame }) {
  const { calculated, players } = useGameState(game);
  if (!players || !calculated) return null;

  const words = calculated.words;

  return (
    <>
      <h5>Game log</h5>
      <ul className="list-unstyled">
        {calculated.log.map((x, index) => (
          <GameLogItem
            key={index}
            item={x}
            players={players}
            words={words}
            isFirst={index === 0}
          />
        ))}
      </ul>
    </>
  );
}

const wordClass = "bg-light small border px-1 rounded";
const wordResultClasses = {
  target: "text-success",
  neutral: "text-muted",
  bomb: "text-dark",
};

function GameLogItem({
  item,
  players,
  words,
  isFirst,
}: {
  item: CalculatedLogItem;
  players: readonly Player<DesignationGameData>[];
  words: Word[];
  isFirst: boolean;
}) {
  const player = <strong>{players[item.playerId]?.name}</strong>;

  switch (item.type) {
    case "hint":
      return (
        <li className={isFirst ? undefined : "mt-2 pt-2 border-top"}>
          {player} gave hint{" "}
          <strong className={wordClass}>
            {item.hint} {item.number}
          </strong>
        </li>
      );

    case "guess":
      return (
        <li className="ms-3">
          {player} guessed{" "}
          <strong className={`${wordClass} ${wordResultClasses[item.result]}`}>
            {words[item.wordIndex].word.toLocaleUpperCase()}
          </strong>
        </li>
      );

    case "endGuessing":
      return <li className="ms-3">{player} ended guessing</li>;

    default: {
      const dummy: never = item;
      return dummy;
    }
  }
}
