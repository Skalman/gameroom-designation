import type { WebSocketStatus } from "@game-room/client";

import type { DesignationGame } from "../DesignationGame";
import { useGameState } from "../useGameState";
import * as Ui from "./ui";

export function Errors({ game }: { game: DesignationGame }) {
  return (
    <>
      <WsError game={game} />
      <ErrorMessages game={game} />
    </>
  );
}

const wsStatusMessages: Record<WebSocketStatus, string | undefined> = {
  connecting: "Connecting...",
  open: undefined,
  waitingForReconnect: "Connection failed. Waiting to reconnect...",
  closing: "Closing connection",
  closed: "No connection",
};

function WsError({ game }: { game: DesignationGame }) {
  const status = useGameState(game, (x) => x.webSocketStatus);
  const message = wsStatusMessages[status];
  if (!message) return null;

  return (
    <Ui.ToastContainer position="top">
      <Ui.Toast header="Connection">{message}</Ui.Toast>
    </Ui.ToastContainer>
  );
}

function ErrorMessages({ game }: { game: DesignationGame }) {
  const errors = useGameState(game, (x) => x.error);
  if (!errors) return null;

  return (
    <Ui.ToastContainer position="topEnd">
      <Ui.Toast header="Error" onClose={() => game.dismissErrors()}>
        {errors.map((x, i) => (
          <p key={i}>{x}</p>
        ))}
      </Ui.Toast>
    </Ui.ToastContainer>
  );
}
