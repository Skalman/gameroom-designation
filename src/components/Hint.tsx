import { useState } from "react";

import type { DesignationGame } from "../DesignationGame";
import type { FsmState } from "../fsm";
import type { CurrentTurn } from "../models";
import { useGameState } from "../useGameState";
import * as Ui from "./ui";

export function Hint({ game }: { game: DesignationGame }) {
  const { state, calculated } = useGameState(game);

  const youGiveHint = state === "giveHint" || state === "youGiveHint";

  if (youGiveHint) {
    return <HintForm game={game} />;
  } else {
    return (
      <CurrentHintAndEndGuessing
        game={game}
        state={state}
        currentTurn={calculated?.currentTurn}
      />
    );
  }
}

function CurrentHintAndEndGuessing({
  game,
  state,
  currentTurn,
}: {
  game: DesignationGame;
  state: FsmState;
  currentTurn?: CurrentTurn;
}) {
  return (
    <div className="mb-3 d-flex justify-content-center">
      {currentTurn && (
        <div className="d-flex align-items-center me-3 fs-4 fw-bold bg-light px-3 py-1 border border-5 border-success rounded">
          {currentTurn.hint} {currentTurn.number}
        </div>
      )}
      {state === "youGuess" && (
        <Ui.ButtonPrimaryLarge
          className="ms-3"
          type="button"
          onClick={() => game.endGuessing()}
        >
          End guessing
        </Ui.ButtonPrimaryLarge>
      )}
    </div>
  );
}

function HintForm({ game }: { game: DesignationGame }) {
  const [hint, setHint] = useState("");
  const [number, setNumber] = useState<"" | number>("");
  const isValid =
    hint !== "" && Number.isInteger(number) && 1 <= number && number <= 7;

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        game.giveHint(hint.toLocaleUpperCase(), number as number);
      }}
      className="hintform input-group mb-3 justify-content-center"
    >
      <Ui.Input
        placeholder="Hint"
        value={hint}
        onChange={(e) => setHint(e.target.value)}
        className="hintform-hint fw-bold"
      />
      <Ui.Input
        className="hintform-number fw-bold"
        placeholder="Number"
        type="number"
        min={1}
        max={7}
        value={number}
        onChange={(e) => setNumber(e.target.value && +e.target.value)}
      />
      <Ui.ButtonPrimary type="submit" disabled={!isValid}>
        Give hint
      </Ui.ButtonPrimary>
    </form>
  );
}
