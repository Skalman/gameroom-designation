export function shuffle<T>(arr: T[]) {
  arr = arr.slice();
  for (let i = 0; i < arr.length; i++) {
    const rand = random(i, arr.length);
    [arr[i], arr[rand]] = [arr[rand], arr[i]];
  }
  return arr;
}

function random(min: number, max: number) {
  return min + Math.floor(Math.random() * (max - min));
}
