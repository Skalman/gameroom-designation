import type { FsmState } from "../fsm";

const messages: Record<FsmState, [string?, string?]> = {
  init: [],
  connecting: ["Connecting to game..."],
  lobby: [],
  joiningTeam: ["Joining team..."],
  playing: ["Preparing game..."],
  giveHint: ["Somebody should give a hint", "What connects the green cards?"],
  youGiveHint: [
    "Your team should give a hint",
    "What connects the green cards?",
  ],
  youGuess: [
    "Your team should guess",
    "Regardless of color, which cards are related to the hint?",
  ],
  theyGiveHint: ["The other team should give a hint"],
  theyGuess: ["The other team is guessing"],
  win: ["You win! 🥳"],
  lose: ["You lose. Better luck next time!"],
};

export function GameStateDisplay({ state }: { state: FsmState }) {
  const [main, explanation] = messages[state];

  return (
    <div className="mb-3">
      {main && <div className="fs-3 fw-bold text-center">{main}</div>}
      {explanation && (
        <div className="h6 text-muted text-center">{explanation}</div>
      )}
    </div>
  );
}
