import { useEffect, useState } from "react";

import { DesignationGame } from "../DesignationGame";
import { Errors } from "./Errors";
import { Header } from "./Header";
import { Main } from "./Main";
import * as Ui from "./ui";

export function Designation({ url }: { url: string }) {
  const [game, setGame] = useState<DesignationGame>();

  useEffect(() => {
    const game = createGame(url);
    setGame(game);
    return () => game.dispose();
  }, [url]);

  if (!game) return null;

  return (
    <>
      <Header game={game} />
      <Ui.Container>
        <Errors game={game} />
        <Main game={game} />
      </Ui.Container>
    </>
  );
}

function createGame(url: string) {
  return new DesignationGame({
    webSocketUrl: url,
    getSlug: () => {
      const url = new URL(window.location.href);
      const urlRoom = url.searchParams.get("room");
      return urlRoom ?? undefined;
    },
    setSlug: (slug) => {
      const url = new URL(window.location.href);
      if (slug) {
        url.searchParams.set("room", slug);
      } else {
        url.searchParams.delete("room");
      }

      window.history.replaceState(null, window.document.title, url.href);
    },
    getState: () => {
      const json = window.localStorage.getItem("designation-words");
      if (json) {
        return JSON.parse(json);
      }
    },
    setState: (state) =>
      window.localStorage.setItem("designation-words", JSON.stringify(state)),
  });
}
