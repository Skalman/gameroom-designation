import { GameRoom, Listener, ListenerCollection } from "@game-room/client";
import { produce } from "immer";

import { arrayToggle } from "./arrayToggle";
import { calculateState } from "./calculateState";
import { otherTeam, wordLists } from "./data";
import { FsmEvent, makeFsm } from "./fsm";
import type {
  DesignationGameData,
  DesignationGameOptions,
  GameState,
  RoomPublicData,
  WordList,
} from "./models";
import { shuffle } from "./shuffle";

export class DesignationGame {
  private readonly room: GameRoom<DesignationGameData>;
  private readonly fsm = makeFsm();
  private readonly stateListeners = new ListenerCollection<GameState>();
  state: GameState = {
    state: this.fsm.current,
    wordList: {
      selected: ["en"],
      ownWords: [],
      additionalWordsRequired: 0,
    },
    webSocketStatus: "closed",
  };

  private set(action: (state: GameState) => void) {
    const nextState = produce(this.state, (x) => {
      action(x);

      if (this.fsm.runPlayingTransition.can()) {
        const calc = calculateState(x);
        if ("error" in calc) {
          x.error ??= [];
          x.error.push(calc.error);
        } else {
          this.fsm[calc.transition]();
          x.calculated = calc.calculated;
          x.state = this.fsm.current;
        }
      }
    });

    if (nextState !== this.state) {
      this.state = nextState;
      this.stateListeners.next(nextState);
    }
  }

  addListener(listener: Listener<GameState>) {
    this.stateListeners.addListener(listener);
  }

  removeListener(listener: Listener<GameState>) {
    this.stateListeners.removeListener(listener);
  }

  private addError(error: string) {
    this.set((x) => {
      x.error ??= [];
      if (!x.error.includes(error)) {
        x.error.push(error);
      }
    });
  }

  dismissErrors() {
    this.set((x) => {
      x.error = undefined;
    });
  }

  private transition(event: FsmEvent) {
    this.fsm[event]();
    this.set((x) => {
      x.state = this.fsm.current;
    });
  }

  constructor(private readonly options: DesignationGameOptions) {
    this.room = this.createRoomInternal(options);

    const storedState = options.getState();
    if (storedState) {
      this.prepareRoom({
        selectedWordLists: storedState.wordLists,
        ownWords: storedState.wordListOwn,
      });
    }

    const slug = options.getSlug();
    if (slug) {
      this.joinRoom(slug);
    }
  }

  dispose() {
    this.room.dispose();
  }

  private createRoomInternal(options: DesignationGameOptions) {
    const room = new GameRoom<DesignationGameData>({
      webSocket: options.webSocketUrl,
    });

    room
      .on(
        "stateChange",
        ({
          publicLog,
          myInfo,
          players,
          publicData,
          connection,
          webSocketStatus,
        }) => {
          this.set((x) => {
            if (players) {
              x.players = room.players;
              x.playersByTeam = {
                a: room.players.filter(
                  (x) => x.isActive && x.publicData?.team === "a"
                ),
                b: room.players.filter(
                  (x) => x.isActive && x.publicData?.team === "b"
                ),
              };
            }

            if (publicLog) x.roomPublicLog = room.publicLog?.items;
            if (publicData) x.roomPublicData = room.publicData?.payload;
            if (connection) {
              x.slug = room.connection.slug;
              this.options.setSlug(x.slug);
            }
            if (myInfo) {
              x.me = undefined;
              if (room.myInfo) {
                const { playerId, playerName, publicData } = room.myInfo;
                if (playerName && publicData) {
                  x.me = {
                    playerId: playerId,
                    name: playerName,
                    team: publicData.team,
                    otherTeam: otherTeam[publicData.team],
                  };
                }
              }
            }
            if (webSocketStatus) {
              x.webSocketStatus = room.webSocketStatus;
            }
          });
        }
      )
      .on("error", (e) => {
        this.addError(e.reason);
      });

    return room;
  }

  prepareRoom(opts: { selectedWordLists?: WordList[]; ownWords?: string[] }) {
    this.transition("prepareRoom");

    this.set((x) => {
      x.wordList.selected = opts.selectedWordLists ?? x.wordList.selected;
      x.wordList.ownWords = opts.ownWords ?? x.wordList.ownWords;
      const words = this.getShuffledWordsFromWordLists(
        x.wordList.selected,
        x.wordList.ownWords
      );
      x.wordList.additionalWordsRequired = 25 - words.length;

      this.options.setState({
        wordLists: x.wordList.selected,
        wordListOwn: x.wordList.ownWords,
      });
    });
  }

  async createRoom() {
    this.transition("createRoom");

    try {
      await this.ensureOpen();
      this.room.create();
      await Promise.all([
        this.room.getNextJoinInfo(),
        this.room.getNextRoomInfo(),
      ]);
      await this.room
        .setRoomInfo({ publicData: this.generateRoomInfo() })
        .getNextRoomInfo();

      this.transition("connectSucceeded");
    } catch (e) {
      this.addError("Failed to create room" + e);
      this.transition("connectFailed");
    }
  }

  private getShuffledWordsFromWordLists(
    wordListCodes: WordList[],
    ownWords: string[]
  ): string[] {
    const usedLists = new Set(
      wordListCodes.map((x) => {
        if (x === "own") {
          return shuffle(ownWords);
        } else {
          return shuffle(wordLists[x]);
        }
      })
    );

    const selectedWords = new Set<string>();

    while (selectedWords.size < 25 && usedLists.size !== 0) {
      for (const list of Array.from(usedLists)) {
        const word = list.pop();
        if (word) {
          selectedWords.add(word);
        } else {
          usedLists.delete(list);
        }
      }
    }

    return Array.from(selectedWords);
  }

  private generateRoomInfo(): RoomPublicData {
    const words = this.getShuffledWordsFromWordLists(
      this.state.wordList.selected,
      this.state.wordList.ownWords
    );
    const numbers = shuffle(words.map((_, i) => i));

    return {
      totalTurns: 9,
      words,
      teams: {
        a: {
          bombs: [0, 1, 2].map((x) => numbers[x]),
          targets: [3, 4, 5, 6, 7, 8, 9, 10, 11].map((x) => numbers[x]),
        },
        b: {
          bombs: [0, 3, 12].map((x) => numbers[x]),
          targets: [1, 4, 5, 6, 13, 14, 15, 16, 17].map((x) => numbers[x]),
        },
      },
    };
  }

  private async ensureOpen() {
    await this.room.getNextStatus("open");
  }

  async joinRoom(slug: string) {
    this.transition("joinRoom");

    try {
      await this.ensureOpen();
      this.room.join(slug);

      await Promise.all([
        this.room.getNextJoinInfo(),
        this.room.getNextRoomInfo(),
      ]);

      this.transition("connectSucceeded");
    } catch (e) {
      this.addError("Failed to join room" + e);
      this.transition("connectFailed");
    }
  }

  async joinTeam(nickname: string, team: "a" | "b") {
    this.transition("joinTeam");

    try {
      this.room.setMyInfo(nickname, { team });

      await this.stateListeners.getNext(
        (x) => x.me?.team === team,
        this.room.connection.timeout ?? 10_000
      );
      this.transition("joinTeamSucceeded");
    } catch (e) {
      this.addError("Failed to join team");
      this.transition("joinTeamFailed");
    }
  }

  giveHint(hint: string, number: number) {
    this.fsm.giveHint();
    if (!this.state.me) {
      throw new Error("Internal error: no info about me");
    }

    this.room.setRoomInfo({
      publicLog: {
        payload: { type: "hint", hint, number, team: this.state.me.team },
      },
    });
  }

  toggleConsideration(word: string) {
    this.fsm.toggleConsideration();
    const myPublicData = this.room.myInfo?.publicData;
    if (!myPublicData) {
      throw new Error("Internal error: missing data: myInfo.publicData");
    }

    const { me, players, roomPublicData, calculated } = this.state;
    const currentTurn = calculated?.currentTurn;
    if (!me || !players || !roomPublicData || !currentTurn) {
      throw new Error(
        "Internal error: missing data: one of {me, players, roomPublicData, currentTurn}"
      );
    }

    const wordIndex = roomPublicData.words.indexOf(word);
    const newMyPublicData = produce(myPublicData, (x) => {
      if (x.considering?.forTurn === currentTurn.index) {
        x.considering.words = arrayToggle(x.considering.words, wordIndex);
      } else {
        x.considering = {
          forTurn: currentTurn.index,
          words: [wordIndex],
        };
      }
    });

    this.room.setMyInfo(me.name, newMyPublicData);
  }

  guess(word: string) {
    this.fsm.guess();
    const { me, roomPublicData } = this.state;
    if (!me || !roomPublicData) {
      throw new Error(
        "Internal error: missing data: one of {me, roomPublicData}"
      );
    }

    const wordIndex = roomPublicData.words.indexOf(word);
    if (wordIndex === -1) {
      throw new Error(`Internal error: word '${word}' doesn't exist`);
    }

    this.room.setRoomInfo({
      publicLog: {
        payload: { type: "guess", wordIndex, team: me.team },
      },
    });
  }

  endGuessing() {
    this.fsm.endGuessing();
    if (!this.state.me) {
      throw new Error("Internal error: no info about me");
    }

    this.room.setRoomInfo({
      publicLog: {
        payload: { type: "endGuessing", team: this.state.me.team },
      },
    });
  }
}
