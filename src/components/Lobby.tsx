import type { Player } from "@game-room/client";
import { useMemo, useState } from "react";

import type { DesignationGame } from "../DesignationGame";
import type { RoomPublicData, Word } from "../models";
import type { DesignationGameData } from "../models";
import { useGameState } from "../useGameState";
import { PlayerList } from "./PlayerList";
import * as Ui from "./ui";
import { WordTable } from "./WordTable";

export function Lobby({ game }: { game: DesignationGame }) {
  const { roomPublicData, players } = useGameState(game);
  if (!roomPublicData || !players) return null;
  return (
    <LobbyImplementation
      game={game}
      roomPublicData={roomPublicData}
      players={players}
    />
  );
}

function LobbyImplementation({
  game,
  roomPublicData,
  players,
}: {
  game: DesignationGame;
  roomPublicData: RoomPublicData;
  players: readonly Player<DesignationGameData>[];
}) {
  const words = useMemo(() => {
    return roomPublicData.words.map<Word>((word) => ({
      word,
      display: "neutral",
      guesses: [],
      isConsumed: false,
      isConsumedForTeams: [],
      considering: [],
    }));
  }, [roomPublicData.words]);

  const [name, setName] = useState("");

  return (
    <div>
      <Ui.Row className="justify-content-center">
        <Ui.Col size={4} className="text-center">
          <Ui.Input
            label="Nickname"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Ui.Col>
      </Ui.Row>
      <Ui.Row className="justify-content-center text-center">
        <Ui.Col size={2}>
          <div className="mb-3">
            <Ui.ButtonPrimary
              type="button"
              disabled={!name}
              onClick={() => game.joinTeam(name, "a")}
            >
              Join team A
            </Ui.ButtonPrimary>
          </div>
          <PlayerList
            players={players.filter(
              (x) => x.isActive && x.publicData?.team === "a"
            )}
          />
        </Ui.Col>
        <Ui.Col size={2}>
          <div className="mb-3">
            <Ui.ButtonPrimary
              type="button"
              disabled={!name}
              onClick={() => game.joinTeam(name, "b")}
            >
              Join team B
            </Ui.ButtonPrimary>
          </div>
          <PlayerList
            players={players.filter(
              (x) => x.isActive && x.publicData?.team === "b"
            )}
          />
        </Ui.Col>
      </Ui.Row>
      <WordTable game={game} words={words} />
    </div>
  );
}
